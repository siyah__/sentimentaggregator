package za.co.omnipresent.sentimentaggregator.service;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import za.co.omnipresent.sentimentaggregator.models.*;
import za.co.omnipresent.sentimentaggregator.repository.*;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AggregatorServiceImplTests {
    private AggregatorServiceImpl aggregatorService;
    @Autowired
    private CitiesRepository citiesRepository;
    @Autowired
    private TweetsWithAnalysisRepository tweetsWithAnalysisRepository;
    @Autowired
    private LocationsRepository locationsRepository;
    @Autowired
    private OverallCountrySentimentRepository overallCountrySentimentRepository;
    @Autowired
    private MongoTemplate mongoTemplate;

    @Before
    public void setUp(){
        aggregatorService = new AggregatorServiceImpl();
        MockitoAnnotations.initMocks(this);
        aggregatorService.setOverallCountrySentimentRepository(this.overallCountrySentimentRepository);
        aggregatorService.setCitiesRepository(this.citiesRepository);
        aggregatorService.setTweetsWithAnalysisRepository(this.tweetsWithAnalysisRepository);
        aggregatorService.setLocationsRepository(this.locationsRepository);
        saveTweetsWithAnalysisToRepo();
        saveLocationsToRepo();
        saveCitiesToRepo();
        saveOverallCountrySentiments();
    }

    @After
    public void clean(){
        mongoTemplate.dropCollection(TweetsWithAnalysisRepository.class);
        mongoTemplate.dropCollection(LocationsRepository.class);
        mongoTemplate.dropCollection(CitiesRepository.class);
        mongoTemplate.dropCollection(OverallCountrySentimentRepository.class);
    }

    @Test
    @DisplayName("Given a list of tweets with analysis, when getOverallSentiment method is called, then create an average based on scores then return the average")
    public void shouldGetOverallSentiment(){
        double actual = aggregatorService.getOverallSentiment(this.tweetsWithAnalysisRepository.findAll());
        double exp = 60.0;
        Assert.assertEquals(exp, actual, 0);
    }

    @Test
    @DisplayName("Given a topic, when aggregateInformation method is called, should create and return an overallCountry Sentiment object")
    public void shouldAggregateInformation(){
        OverallCountrySentiment overallCountrySentiment = aggregatorService.aggregateInformation("Cars");
        Assert.assertEquals("South Africa", overallCountrySentiment.getCountryName());
        Assert.assertEquals("Cars", overallCountrySentiment.getTopic());
        Assert.assertEquals(60.0, overallCountrySentiment.getOverallSentiment(), 0);
        Assert.assertFalse(overallCountrySentiment.getProvinceSentiments().isEmpty());
        Assert.assertFalse(overallCountrySentiment.getProvinceSentiments().get(0).getCitySentiments().isEmpty());
    }

    @Test
    @DisplayName("Given a topic, when findSenitmentByCity method is called, should create and return a LocationSentiment list")
    public void shouldFindSentimentByCity(){
        List<CitySentiment> citySentiments = aggregatorService.findSentimentByCity("Cars", "Gauteng" );
        CitySentiment citySentiment = citySentiments.get(0);
        Assert.assertEquals("South Africa", citySentiment.getCountryName());
        Assert.assertEquals("Gauteng",citySentiment.getProvinceName());
        Assert.assertEquals("Pretoria",citySentiment.getCityName());
        Assert.assertEquals(70.00, citySentiment.getOverallSentiment(), 0);
        Assert.assertEquals(4, citySentiments.size());
    }
    @Test
    @DisplayName("Given a topic, when findSentimentByProvince method is called, should create and return a LocationSentiment list")
    public void shouldFindSentimentByProvince(){
        List<ProvinceSentiment> provinceSentiments = aggregatorService.findSentimentByProvince("Cars");
        ProvinceSentiment provinceSentiment = provinceSentiments.get(0);
        Assert.assertEquals("South Africa",provinceSentiment.getCountryName());
        Assert.assertEquals("Gauteng",provinceSentiment.getProvinceName());
        Assert.assertEquals(60.00, provinceSentiment.getOverallSentiment(), 0);
        Assert.assertTrue(provinceSentiment.getCitySentiments().size()> 1);
    }
    @Test
    @DisplayName("Given a topic, when findSentimentByCountry method is called, should create and return an LocationSentiment list")
    public void shouldFindSentimentByCountry(){
        LocationSentiment citySentiment = aggregatorService.findSentimentByCountry("Cars", "South Africa");
        Assert.assertEquals("South Africa",citySentiment.getCountryName());
        Assert.assertEquals("Cars",citySentiment.getTopic());
        Assert.assertNull(citySentiment.getProvince());
        Assert.assertNull(citySentiment.getCityName());
        Assert.assertEquals(60.00, citySentiment.getOverallSentiment(), 0);
    }


    public void saveOverallCountrySentiments (){
        List<ProvinceSentiment> provinceSentiments = createProvinceSentimentsList();

        this.overallCountrySentimentRepository.save(new OverallCountrySentiment("South Africa", "Cars", 50.00, provinceSentiments));
    }

    public List<ProvinceSentiment> createProvinceSentimentsList(){
        List<ProvinceSentiment> provincesList = new ArrayList<>();
        provincesList.add(new ProvinceSentiment("Gauteng", "South Africa", 60.00, createCitySentimentsList()));
        provincesList.add(new ProvinceSentiment("Western Cape", "South Africa", 30.00, createCitySentimentsList1()));
        return provincesList;
    }
    public List<CitySentiment> createCitySentimentsList(){
        List<CitySentiment> citiesList = new ArrayList<>();
        citiesList.add(new CitySentiment("Johannesburg", "Gauteng", "Cars", 20.00));
        citiesList.add(new CitySentiment("Pretoria", "Pretoria", "Cars", 10.00));

        return citiesList;
    }
    public List<CitySentiment> createCitySentimentsList1(){
        List<CitySentiment> citiesList = new ArrayList<>();
        citiesList.add(new CitySentiment("Stellenbosch", "Western Cape", "South Africa", 20.00));
        citiesList.add(new CitySentiment("Cape Town", "Western Cape", "South Africa", 20.00));

        return citiesList;
    }



    public void saveLocationsToRepo(){
        List<Locations> locationsList = new ArrayList<>();
        Locations location = new Locations("Gauteng", "23.222,22.234,4km");
        locationsList.add(location);
        Locations location1 = new Locations("Western Cape", "13.333,11.223,10km");
        locationsList.add(location1);
        this.locationsRepository.saveAll(locationsList);
    }

    public void saveCitiesToRepo(){
        List<Cities> listOfCities = new ArrayList<>();
        listOfCities.add(new Cities(
                "Pretoria",
                "Gauteng",
                "23.5564,24.24645,25km"
        ));
        listOfCities.add(new Cities(
                "Johannesburg",
                "Gauteng",
                "23.52312,26.24645,22km"
        ));
        this.citiesRepository.saveAll(listOfCities);
    }

    public void saveTweetsWithAnalysisToRepo(){
        List<TweetsWithAnalysis> tweetsWithAnalysisList = new ArrayList<>();

        tweetsWithAnalysisList.add(new TweetsWithAnalysis(
                "Cars",
                "cars are such amazing machines",
                0.5,
                "Gauteng",
                "Johannesburg",
                "South Africa"
        ));
        tweetsWithAnalysisList.add(new TweetsWithAnalysis(
                "Cars",
                "I love cars, I even just bought a new one last week",
                0.7,
                "Gauteng",
                "Pretoria",
                "South Africa"
        ));
        this.tweetsWithAnalysisRepository.saveAll(tweetsWithAnalysisList);
    }
}
