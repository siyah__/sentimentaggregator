package za.co.omnipresent.sentimentaggregator.service;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import za.co.omnipresent.sentimentaggregator.models.*;
import za.co.omnipresent.sentimentaggregator.repository.*;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

@Log4j2
public class AggregatorServiceImpl implements AggregatorService
{
    private CitiesRepository citiesRepository;
    private TweetsWithAnalysisRepository tweetsWithAnalysisRepository;
    private LocationsRepository locationsRepository;
    private OverallCountrySentimentRepository overallCountrySentimentRepository;

    @Override
    public OverallCountrySentiment aggregateInformation(String topic)
    {
        LocationSentiment locationSentiment = findSentimentByCountry(topic, "South Africa");

        return overallCountrySentimentRepository.save(new OverallCountrySentiment(
                locationSentiment.getCountryName(),
                locationSentiment.getTopic(),
                locationSentiment.getOverallSentiment(),
                findSentimentByProvince(topic)
        ));
    }

    public List<CitySentiment> findSentimentByCity(String topic, String provinceName){
        List<CitySentiment> citySentiments = new ArrayList<>();
        List<Cities> citiesList = citiesRepository.findByProvince(provinceName);

        for(Cities city: citiesList){
            List<TweetsWithAnalysis> tweetsList = tweetsWithAnalysisRepository.findByTopicAndCityName(topic, city.getCityName());
            if(!tweetsList.isEmpty())
            {
                citySentiments.add(new CitySentiment(city.getCityName(), city.getProvince(), "South Africa", getOverallSentiment(tweetsList)));
            }
        }
        return citySentiments;
    }

    public List<ProvinceSentiment> findSentimentByProvince(String topic){
        List<Locations> locationsList = locationsRepository.findAll();
        List<ProvinceSentiment> provinceSentiments = new ArrayList<>();

        for(Locations location: locationsList)
        {
            List<TweetsWithAnalysis> tweetsWithAnalysis = tweetsWithAnalysisRepository.findByTopicAndProvince(topic, location.getProvince());
            if (!tweetsWithAnalysis.isEmpty()){
                provinceSentiments.add(new ProvinceSentiment(location.getProvince(), "South Africa", getOverallSentiment(tweetsWithAnalysis), findSentimentByCity(topic, location.getProvince())));
            }
        }
        return provinceSentiments;
    }

    public LocationSentiment findSentimentByCountry(String topic, String countryName){
        List<TweetsWithAnalysis> tweetsWithAnalysis = tweetsWithAnalysisRepository.findByTopicAndCountryName(topic, countryName);
        if(!tweetsWithAnalysis.isEmpty()){
            return new LocationSentiment(null, null, topic, getOverallSentiment(tweetsWithAnalysis), countryName);
        }
        return null;
    }

    public double getOverallSentiment(List<TweetsWithAnalysis> tweetsList)
    {
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        int count =0;
        double total = 0;

        for(TweetsWithAnalysis tweets: tweetsList)
        {
            total += tweets.getScore();
            count++;
        }

        return Double.parseDouble(decimalFormat.format(total/count * 100));
    }

    @Autowired
    public void setTweetsWithAnalysisRepository(TweetsWithAnalysisRepository tweetsWithAnalysisRepository){
        this.tweetsWithAnalysisRepository = tweetsWithAnalysisRepository;
    }

    @Autowired
    public void setCitiesRepository(CitiesRepository citiesRepository) {
        this.citiesRepository = citiesRepository;
    }

    @Autowired
    public void setLocationsRepository(LocationsRepository locationsRepository){
        this.locationsRepository = locationsRepository;
    }

    @Autowired
    public void setOverallCountrySentimentRepository(OverallCountrySentimentRepository overallCountrySentimentRepository){
        this.overallCountrySentimentRepository = overallCountrySentimentRepository;
    }
}
