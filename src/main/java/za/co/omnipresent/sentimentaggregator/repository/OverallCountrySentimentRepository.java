package za.co.omnipresent.sentimentaggregator.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.omnipresent.sentimentaggregator.models.OverallCountrySentiment;

public interface OverallCountrySentimentRepository extends MongoRepository<OverallCountrySentiment, String> {

}
