package za.co.omnipresent.sentimentaggregator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SentimentAggregatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(SentimentAggregatorApplication.class, args);
	}

}
