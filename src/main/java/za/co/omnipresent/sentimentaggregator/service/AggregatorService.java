package za.co.omnipresent.sentimentaggregator.service;

import za.co.omnipresent.sentimentaggregator.models.OverallCountrySentiment;

public interface AggregatorService {
    OverallCountrySentiment aggregateInformation(String topic);
}
