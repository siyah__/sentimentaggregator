package za.co.omnipresent.sentimentaggregator.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.omnipresent.sentimentaggregator.models.Locations;

public interface LocationsRepository extends MongoRepository<Locations, String> {
}
