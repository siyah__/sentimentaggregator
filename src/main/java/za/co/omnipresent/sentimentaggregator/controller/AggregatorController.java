package za.co.omnipresent.sentimentaggregator.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import za.co.omnipresent.sentimentaggregator.models.OverallCountrySentiment;
import za.co.omnipresent.sentimentaggregator.service.AggregatorService;

@RestController
@RequestMapping(value = "/aggregator")
public class AggregatorController {

    private AggregatorService aggregatorService;

    @GetMapping(value = "aggregate/sentiment/{topic}")
    public @ResponseBody OverallCountrySentiment aggregateInfo(@PathVariable String topic)
    {
       return aggregatorService.aggregateInformation(topic);
    }

    @Autowired
    public void setAggregatorService(AggregatorService aggregatorService){
        this.aggregatorService = aggregatorService;
    }
}
