package za.co.omnipresent.sentimentaggregator.models;

import lombok.Data;

@Data
public class Locations {
    private String province;
    private String coordinates;

    public Locations(String province, String coordinates) {
        this.province = province;
        this.coordinates = coordinates;
    }
}
