package za.co.omnipresent.sentimentaggregator.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import za.co.omnipresent.sentimentaggregator.service.AggregatorServiceImpl;

@Configuration
public class BeanConfig {

    @Bean
    public AggregatorServiceImpl aggregatorService() {
        return new AggregatorServiceImpl();
    }
}
