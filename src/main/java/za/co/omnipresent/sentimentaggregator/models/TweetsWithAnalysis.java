package za.co.omnipresent.sentimentaggregator.models;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.io.Serializable;

@Data
public class TweetsWithAnalysis implements Serializable {
    @Id
    private String id;

    private String topic;
    private String fullText;
    private double score;
    private String province;
    private String cityName;
    private String countryName;


    public TweetsWithAnalysis(String topic, String fullText, double score, String province, String cityName, String countryName){
        this.topic = topic;
        this.fullText = fullText;
        this.score = score;
        this.province = province;
        this.cityName = cityName;
        this.countryName = countryName;
    }
}
