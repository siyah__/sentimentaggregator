package za.co.omnipresent.sentimentaggregator.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.omnipresent.sentimentaggregator.models.TweetsWithAnalysis;

import java.util.List;

public interface TweetsWithAnalysisRepository extends MongoRepository<TweetsWithAnalysis, String> {
    List<TweetsWithAnalysis> findByTopicAndCityName(String topic, String cityName);
    List<TweetsWithAnalysis> deleteByTopic(String topic);
    List<TweetsWithAnalysis> findByTopicAndProvince(String topic, String province);
    List<TweetsWithAnalysis> findByTopicAndCountryName(String topic, String countryName);
}
