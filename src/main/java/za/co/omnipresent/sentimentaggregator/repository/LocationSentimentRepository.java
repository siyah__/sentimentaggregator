package za.co.omnipresent.sentimentaggregator.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.omnipresent.sentimentaggregator.models.LocationSentiment;

public interface LocationSentimentRepository extends MongoRepository<LocationSentiment, String> {

}
