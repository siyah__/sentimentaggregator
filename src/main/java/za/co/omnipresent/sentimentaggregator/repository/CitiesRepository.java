package za.co.omnipresent.sentimentaggregator.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import za.co.omnipresent.sentimentaggregator.models.Cities;

import java.util.List;

public interface CitiesRepository extends MongoRepository<Cities, String> {
    List<Cities> findByProvince(String province);
}
